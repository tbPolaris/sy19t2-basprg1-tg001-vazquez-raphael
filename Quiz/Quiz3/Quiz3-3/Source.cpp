#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main()
{
	srand(time(0));
	int diceOne = 1;
	int diceTwo = 1;
	int perc = 1;
	int attempt = 0;
	cout << "Press any key to roll two dice" << endl;
	while (perc != 0)
	{
		system("pause");
		diceOne = rand() % 6 + 1;
		diceTwo = rand() % 6 + 1;
		cout << diceOne << ", " << diceTwo << endl;

		perc = (diceOne + diceTwo) % 4;
		
		attempt += 1;
	}
	cout << "You rolled " << attempt << " times." << endl;

	system("pause");
	return 0;
}