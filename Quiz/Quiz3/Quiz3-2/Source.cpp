#include <iostream>

using namespace std;

int main()
{
	int piece = 1;
	for (int i = 0; i < 6; i++)
	{
		
		int row = 0;
		while (row < 8)
		{
			piece %= 2;
			if (piece == 1)
			{
				cout << "X ";
			}
			else
			{
				cout << "O ";
			}
			row += 1;
			piece += 1;
		}
		cout << "" << endl;
		piece += 1;

	}

	system("pause");
	return 0;
}