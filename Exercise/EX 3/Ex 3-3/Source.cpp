#include <iostream>

using namespace std;
int main()
{
	int start;
	int prog;
	int total;
	int sum = 0;

	cout << "Starting value: ";
	cin >> start;
	cout << "Number of terms: ";
	cin >> prog;

	for (int i = start; i < prog + 1; i++)
	{
		total = i;
		sum = sum + total;
	}

	cout << "Sum: " << sum << endl;

	system("pause");
	return 0;
}