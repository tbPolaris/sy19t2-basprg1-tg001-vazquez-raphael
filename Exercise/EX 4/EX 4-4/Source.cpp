#include <iostream>
#include <string>

using namespace std;


int main()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int inventory = sizeof(items) / sizeof(items[0]);
	string check;
	int itemCheck = 0;
	cout << "Input item name to be checked in inventory: ";
	cin >> check;


	for (int i = 0; i < inventory; i++)
	{
		if (check == items[i])
		{
			itemCheck = i;
			break;
		}

		itemCheck = inventory + 1;
	}

	if (itemCheck < inventory) {
		cout << "item " << check << " exists in slot " << itemCheck + 1 << " of inventory" << endl;
	}
	else
	{
		cout << "item " << check << " does not exist in inventory" << endl;
	}



	system("pause");
	return 0;
}