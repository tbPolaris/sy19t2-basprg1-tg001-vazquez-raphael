#include <iostream>


using namespace std;


int main()
{
	int numbers1[] = { 1, 2, 3, 4, 5 };
	int numbers2[] = { 0, 7, -10, 5, 3 };
	int sum[5];
	int sumNum;
	int arrSize = sizeof(sum) / sizeof(sum[0]);
	
	for (int i = 0; i < arrSize; i++)
	{
		sumNum = numbers1[i] + numbers2[i];
		sum[i] = sumNum;

	}
	cout << "sum of arrays 1 and 2" << endl;
	cout << "{ ";
	for (int r = 0; r < arrSize; r++)
	{
		cout << sum[r];
		if (r < 4)
		{
			cout << ", ";
		}
		else
		{
			cout << " }" << endl;
		}

	}


	system("pause");
	return 0;
}