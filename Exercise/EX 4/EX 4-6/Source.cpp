#include <iostream>
#include <string>
#include <time.h>
#include <cstdlib>

using namespace std;


int main()
{
	srand(time(0));
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	string exit;
	bool gacha = true;
	int inventory = sizeof(items) / sizeof(items[0]);
	int roll = 0;
	cout << "Press enter to randomly search inventory" << endl;
	system("pause");

	while (gacha)
	{
		roll = (rand() % 8) + 1;
		cout << "You found " << items[roll] << " in your inventory" << endl;
		cout << "Continue searching? " << endl;
		cout << "Press x to exit" << endl;
		cin >> exit;
		if (exit == "x" || exit == "X")
		{
			!gacha;
		}
		else
		{
			continue;
		}
	}
	system("pause");
	return 0;
}