#include <iostream>
#include <string>
using namespace std;

int main()
{
	int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };
	int set = sizeof(numbers) / sizeof(numbers[0]);
	int largest = 0;

	for (int i = 0; i < set; i++)
	{
		if (numbers[i] > largest)
		{
			largest = numbers[i];
		}
	}
	cout << largest << " is the largest number in the array" << endl;

	system("pause");
	return 0;
}