#include <iostream>
#include <string>

using namespace std;
int main()
{
	// declaring values
	int hp = 0;
	int dmg = 0;
	int hpCheck = 0;
	float percent = 0.0f;

	cout << "Input your current HP: ";
	cin >> hp;
	cout << "How much damage did you take? ";
	cin >> dmg;

	system("CLS");
	// computing for current HP and percentage
	hpCheck = hp - dmg;
	percent = ((float)hpCheck / hp) * 100;

	cout << "Remaining HP = " << percent << "%";
	cout << "" << endl;

	

	if (percent == 100)
	{
		cout << "You are still at full HP!" << endl;

	}
	else if (percent >= 50)
	{
		cout << "You are still healthy!" << endl;
	}
	else if (percent >= 20)
	{
		cout << "You are in the yellow range." << endl;
	}
	else if (percent >= 1)
	{
		cout << "You are almost dead! Drink a potion!" << endl;
	}
	else
	{
		cout << "You are dead." << endl;
	}


	system("pause");
	return 0;

}