#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <cstdlib>

using namespace std;


int main()
{
	//initializing values and setting time for opponents "AI" 
	srand(time(0));
	int playerHP, oppHP, cPlayerHP, cOppHP, playerATKmin, playerATKmax, oppATKmin, oppATKmax;
	string playerName;
	string enemyName;
	string command;
	int playATKcalc = 0;
	int oppATKcalc = 0;
	
	bool gameEnd = 1;

	//welcome message and player initialization
	cout << "Initializing battle simulation..." << endl;
	system("pause");
	cout << "Please input your name." << endl;
	cin >> playerName;
	cout << "Warrior " << playerName << ", please set your initial HP." << endl;
	cout << "HP value: ";
	cin >> playerHP;
	system("CLS");
	cPlayerHP = playerHP;
	// note 7 spaces between name and HP 
	cout << "Warrior " << playerName << "       HP: " << cPlayerHP << " / " << playerHP << endl;
	cout << " " << endl;
	cout << "Please set your damage output." << endl;
	cout << "Minimum: ";
	cin >> playerATKmin;
	cout << "Maximum: ";
	cin >> playerATKmax;
	system("CLS");
	cout << "Warrior " << playerName << "       HP: " << cPlayerHP << " / " << playerHP << endl;
	cout << " " << endl;
	cout << "Please initialize the opponents name, HP and attack values." << endl;
	cout << "Opponents name: ";
	cin >> enemyName;
	cout << enemyName << "'s HP : ";
	cin >> oppHP;
	cOppHP = oppHP;
	cout << enemyName << "'s Minimum damage: ";
	cin >> oppATKmin;
	cout << enemyName << "'s Maximum damage: ";
	cin >> oppATKmax;
	system("CLS");
	while (gameEnd == 1)
	{
		//calculating random damage value
		int pMinMaxDiff = playerATKmax - playerATKmin;
		int oppMinMaxDiff = oppATKmax - oppATKmin;
		char result = 'g';
		playATKcalc = (rand() % pMinMaxDiff + 1) + playerATKmin;
		oppATKcalc = (rand() % oppMinMaxDiff + 1) + oppATKmin;
		//setting variables for atk history
		int lastATKvalueP;
		int lastATKvalueO;
		int halfATKplayer = playATKcalc / 2;
		int halfATKopp = oppATKcalc / 2;
		int wATKplayer = playATKcalc * 2;
		int wATKopp = oppATKcalc * 2;

		// battle screen
		cout << "Warrior " << playerName << "                                                                    Opponent " << enemyName << endl;
		cout << "HP: " << cPlayerHP << " / " << playerHP << "                                                                     HP: " << cOppHP << " / " << oppHP << endl;
		cout << "" << endl;
		cout << "" << endl;
		cout << "" << endl;
		cout << "" << endl;



		cout << "Strike: Attack the enemy" << endl;
		cout << "Anticipate: Take a defensive stance agaisnt an attack. Can counter a flail." << endl;
		cout << "Flail: A reckless attack that deals twice as much damage." << endl;
		cout << "" << endl;
		cout << "Type in an action(case sensitive): strike, anticipate, flail" << endl;
		cout << "Action: ";
		cin >> command;

		system("CLS");

		//assigning string commands to char values
		char commandIn = 's';
		if (command == "strike") {
			commandIn = 'a';
		}
		else if (command == "anticipate")
		{
			commandIn = 'b';
		}
		else if (command == "flail")
		{
			commandIn = 'c';
		}

		int rando = oppATKmax - oppATKmin;
		int enemyCommand = (rand() % 3);
		

		// checking outcome based on opponents move. note 0 = strike, 1 = anticipate, 2 = flail
		
		switch (commandIn)
		{
		case 'a':
			if (enemyCommand == 0) {
				cPlayerHP = cPlayerHP - oppATKcalc;
				cOppHP = cOppHP - playATKcalc;
				lastATKvalueP = playATKcalc;
				lastATKvalueO = oppATKcalc;
				result = 'q';
			}
			else if (enemyCommand == 1) {
				cOppHP = cOppHP - halfATKplayer;
				result = 'w';
			}
			else if (enemyCommand == 2) {
				cPlayerHP = cPlayerHP - wATKopp;
				cOppHP = cOppHP - playATKcalc;
				result = 'e';
				lastATKvalueO = wATKopp;
				lastATKvalueP = playATKcalc;
			}
			break;
		case 'b':
			if (enemyCommand == 0) {
				cPlayerHP = cPlayerHP - halfATKopp;
				result = 'r';
			}
			else if (enemyCommand == 2) {
				cOppHP = cOppHP - wATKplayer;
				result = 't';
			}
			else if (enemyCommand == 1) {
				result = 'y';
			}
			break;
		case 'c':
			if (enemyCommand == 0) {
				cPlayerHP = cPlayerHP - oppATKcalc;
				cOppHP = cOppHP - wATKplayer;
				result = 'u';
			}
			else if (enemyCommand == 1) {
				cPlayerHP = cPlayerHP - wATKopp;
				result = 'i';
			}
			else if (enemyCommand == 2) {
				cPlayerHP = cPlayerHP - wATKopp;
				cOppHP = cOppHP - wATKplayer;
				result = 'o';
			}
			break;
		default:
			cout << "Invalid command" << endl;
			break;
		}
		switch (result)
		{
		case 'q':
			cout << enemyName << " hits! " << playerName << " takes " << lastATKvalueO << "!" << endl;
			cout << playerName << " hits! " << enemyName << " takes " << lastATKvalueP << "!" << endl;
			cout << "" << endl;
			break;
		case 'w':
			cout << playerName << " hits but " << enemyName << " defends! " << enemyName << " takes " << halfATKplayer << "!" << endl;
			cout << "" << endl;
			cout << "" << endl;
			break;
		case 'e':
			cout << enemyName << " hits! " << playerName << " takes " << lastATKvalueO << "!" << endl;
			cout << playerName << " hits! " << enemyName << " takes " << lastATKvalueP << "!" << endl;
			cout << "" << endl;
			break;
		case 'r':
			cout << enemyName << " hits but" << playerName << " defends! " << playerName << " takes " << halfATKopp << "!" << endl;
			cout << "" << endl;
			cout << "" << endl;
			break;
		case 'y':
			cout << "Both players anticipated an attack, but nothing happened!" << endl;
			cout << "" << endl;
			cout << "" << endl;
			break;
		case 't':
			cout << enemyName << " attacks but " << playerName << " anticipated it! Counter! " << enemyName << " takes " << wATKplayer << "!" << endl;
			cout << "" << endl;
			cout << "" << endl;
			break;
		case 'u':
			cout << enemyName << " hits! " << playerName << " takes " << oppATKcalc << "!" << endl;
			cout << playerName << " flails and hits! " << enemyName << " takes " << wATKplayer;
			cout << "" << endl;
			break;
		case 'i':
			cout << playerName << " flails but " << enemyName << " anticipated it! Counter! " << playerName << " takes " << wATKopp << "!" << endl;
			cout << "" << endl;
			cout << "" << endl;
			break;
		case 'o':
			cout << enemyName << " flails and hits! " << playerName << " takes " << wATKopp << "!" << endl;
			cout << playerName << " flails and hits! " << enemyName << " takes " << wATKplayer << "!" << endl;
			cout << "" << endl;
			break;
		default:
			cout << "" << endl;
			cout << "" << endl;
			cout << "" << endl;
		}
		system("pause");
		system("CLS");

		if (cPlayerHP <= 0 || cOppHP <= 0) {
			gameEnd = 0;
		}

	}

	if (cPlayerHP <= 0 && cOppHP <= 0) {
		cout << "Both warriors have been defeated. DRAW!" << endl;
	}
	else if (cOppHP <= 0){
		cout << enemyName << " Has been defeated. Congratulations!" << endl;
	}
	else if (cPlayerHP <= 0) { 
		cout << "You have been defeated! Game over." << endl;
	}

		
	
	system("pause");
	return 0;
}